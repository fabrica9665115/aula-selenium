import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service


servico = Service(ChromeDriverManager().install())

navegador = webdriver.Chrome(service=servico)

navegador.get("https://www.python.org/")

navegador.find_element('xpath','//*[@id="documentation"]/a').click()
time.sleep(5)

navegador.find_element('xpath','//*[@id="id-search-field"]').send_keys('classes')
navegador.find_element('xpath','//*[@id="submit"]').submit()
time.sleep(20)

